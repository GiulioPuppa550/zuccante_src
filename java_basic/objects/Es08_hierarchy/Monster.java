import java.util.Random;

public abstract class Monster implements Fighter {

    private static Random rand = new Random();

    public String name;
    public int color;
    public int power = 0;

    public Monster(String name, int color) {
        this.name = name;
        this.color = color;
    }

    public static Monster getRandomMonster(String n, int c, int p) {
        if (rand.nextBoolean())
            return new Pterox(n, c, p);
        else
            return new Tyros(n, c, p);
    }

    @Override
    public int fight(Monster enemy) {
        if (power > enemy.power) {
            enemy.power--;
            power++;
            return 1;
        }
        if (power == enemy.power) {
            return 0;
        } else {
            enemy.power++;
            power--;
            return -1;
        }
    }

    // try to uncomment after testing using Test
    @Override
    public final String toString() {
        return this.getClass().getName() + ": " + name;
    }

}