public interface Color {

    int RED = 1;
    int WHITE = 2;
    int BLACK = 3;
    int GREEN = 4;
    int YELLOW = 5;
    int BROWN = 6;
    int BLUE = 7;

}