public class Overriding {
    public static void main(String[] args) {

        A a = new A();
        B b = new B();
        ((A)b).myMethod();
        b.myMethodA();
        b.f1 = 10;
        b.show();
        b.showA();
        
    }
}

class A {

    int f1;
    int f2;

    public void myMethod() {
        System.out.println("inside A");
    }

    public void show() {
        System.out.println("f1 value is: " + f1);   
    }
}

class B extends A {

    int f1;
    int f2;

    public void myMethod() {
        System.out.println("inside B");
    }

    public void myMethodA() {
        super.myMethod();
    }

    public void show() {
        System.out.println("f1 value is: " + f1);   
    }

    public void showA() {
        System.out.println("f1 value is: " + super.f1);   
    }
}