public interface PasswordManager {

    String encode(String pwd);

}
