import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HotelDao {

    public List<String> fetchAvailableRooms() throws SQLException {
        List<String> availableRooms = new ArrayList<>();
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/mockito000", "admin", "zuccante");
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM Rooms WHERE available = 1 ");
        while(rs.next()){
            availableRooms.add(rs.getString("name"));
        }
        return availableRooms;
    }
}
