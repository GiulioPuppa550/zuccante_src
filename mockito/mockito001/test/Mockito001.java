import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.InOrder;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Mockito.*;

public class Mockito001 {

    // mock default values

    @Test
    void mockTest01() {
        Demo demo = mock(Demo.class);

        assertEquals(0, demo.getInt());
        assertEquals(0, demo.getInteger().intValue());
        assertEquals(0d, demo.getDouble(), 0d);
        assertFalse(demo.getBoolean());
        assertNull(demo.getObject());
        assertEquals(Collections.emptyList(), demo.getCollection());
        assertNull(demo.getArray());
        assertEquals(0L, demo.getStream().count());
        assertFalse(demo.getOptional().isPresent());
    }

    // stubbing

    @Test
    void mockTest02() {
        LinkedList mockedList = mock(LinkedList.class);

        when(mockedList.get(0)).thenReturn("first");
        // exception
        when(mockedList.get(1)).thenThrow(new RuntimeException());

        // following prints "first"
        System.out.println(mockedList.get(0));

        // following throws runtime exception
        // System.out.println(mockedList.get(1));

        // following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999));

        // Although it is possible to verify a stubbed invocation, usually it's just redundant
        // If your code cares what get(0) returns, then something else breaks (often even before verify() gets executed).
        // If your code doesn't care what get(0) returns, then it should not be stubbed.
        verify(mockedList).get(0);

    }

    @Test
    void mockTest03() {
        PasswordManager mng = mock(PasswordManager.class);

        when(mng.encode(anyString())).thenReturn("exact");
        assertEquals("exact", mng.encode("1"));
        assertEquals("exact", mng.encode("abc"));

        // do not use inside below
        // String orMatcher = or(eq("a"), endsWith("b"));
        verify(mng).encode(or(eq("a"), endsWith("c"))); // OK

        // time out
        verify(mng, timeout(500)).encode("abc");

        // never happen
        verify(mng, never()).encode("def");

    }

    @Test
    void mockTest04() {
        PasswordManager first = mock(PasswordManager.class);
        PasswordManager second = mock(PasswordManager.class);
        // simulate calls
        first.encode("pw1");
        second.encode("pw1");
        first.encode("pw2");
        // verify call order
        InOrder inOrder = inOrder(first, second);
        inOrder.verify(first).encode("pw1");
        inOrder.verify(second).encode("pw1");
        inOrder.verify(first).encode("pw2");
    }

    @Test
    void mockTest05() {
        List mockedList = mock(List.class);

        // stubbing using built-in anyInt() argument matcher
        when(mockedList.get(anyInt())).thenReturn("element");
        System.out.println(mockedList.get(999));

        // stubbing using custom matcher (let's say isValid() returns your own matcher implementation):
        ArgumentMatcher<String> isValid = string -> string.length() > 2;
        when(mockedList.contains(argThat(isValid))).thenReturn(true);

        // you can also verify using an argument matcher
        verify(mockedList).get(anyInt());

        //argument matchers can also be written as Java 8 Lambdas
        // verify(mockedList).add(argThat((String someString) -> someString.length() > 5));

    }

    // capturing arguments

    @Test
    void mockTest06() {
        PasswordManager mng = mock(PasswordManager.class);
        mng.encode("password1");
        mng.encode("password2");
        mng.encode("password3");
        ArgumentCaptor<String> passwordCaptor = ArgumentCaptor.forClass(String.class);
        verify(mng, times(3)).encode(passwordCaptor.capture());
        assertEquals(List.of("password1", "password2", "password3"),
                passwordCaptor.getAllValues());
    }


}
