import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import java.util.List;

public class Mockito000 {

    List mockedList = mock(List.class);

    @Test
    void mockTest() {
        mockedList.add("one");
        mockedList.clear();
        verify(mockedList).add("one");
        verify(mockedList).clear();
    }
}
