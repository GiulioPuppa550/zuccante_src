# Alcuni dettagli sul codice

`Board` è il motore del gioco

- `ActonListener`: interfaccia pensata per porsi in ascolto - un *obserber* - di `AcionEvent`, sono gli eventi di `awt` ad alto livello, un esempio è il *click* di un bottone o di un tasto del mouse o della tastiera.
- `JPane` in parte lo abbiamo conosciuto 
> The JPanel class provides general-purpose containers for lightweight components.

Il metodo `initBoard()` è un metodo di inizializzazione, al nostro `JPanel` qui `Board`, leghiamo un *timer* - un `javax.Swing.Timer` - che scandisce l'animazione ridisegnando i *frame*
``` java
timer = new Timer(DELAY, this); 
        timer.start();
```

Il metodo
``` java
@Override
public void paintComponent(Graphics g) {
    super.paintComponent(g);

    drawStar(g);
}
```
ci permette di realizzare una *custom paint*, la sua chiamata avviene certamente dopo aver chiamato `repaint()` di `Component`.

- ` Toolkit.getDefaultToolkit().sync()` sincronizza lo stato del *toolkit* in uso: un *toolkit* è responsabile della gestione degli eventi (in modo asincrono); in questo caso lo usaimao com eprecauzione.