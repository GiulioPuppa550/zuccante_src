import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Board extends JPanel
    implements Runnable {

    // windows size
    private final int B_WIDTH = 400;
    private final int B_HEIGHT = 400;

    // frame rate
    private final int DELAY = 25;

    // star initial position and velocity
    private final int INITIAL_X = 120;
    private final int INITIAL_Y = 80;
    private final int VEL = 1;

    private Image star;
    private int x, y;
    private int vel_x, vel_y;

    // the animation thread
    private Thread animator;
    
    public Board() {
        initBoard();
    }

    private void loadImage() {
        ImageIcon ii = new ImageIcon("star.png");
        star = ii.getImage();
    }

    private void initBoard() {
        setBackground(Color.BLACK);
        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));

        loadImage();

        // first position of star
        x = INITIAL_X;
        y = INITIAL_Y;
        // and initial velocity
        vel_x = 1 * VEL;
        vel_y = 1 * VEL;
    }

    // tis method is called when the Board is attached to the parent JFrame
    @Override
    public void addNotify() {
        super.addNotify();
        animator = new Thread(this);
        animator.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawStar(g);
    }

    private void drawStar(Graphics g) {
        g.drawImage(star, x, y, this);
        Toolkit.getDefaultToolkit().sync();
    }

    private void cycle() {
        x += vel_x;
        y += vel_y;
        if (x + star.getWidth(null) > B_WIDTH || x < 0) {
            vel_x = -vel_x;
        }
        if (y + star.getHeight(null) > B_HEIGHT || y < 0) {
            vel_y = -vel_y;
        }
    }

    /* ORIGINAL VERSION
    // the animation task 
    @Override
    public void run() {

        long beforeTime, timeDiff, sleep;

        beforeTime = System.currentTimeMillis();

        while (true) {

            cycle();
            repaint();

            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = DELAY - timeDiff;

     
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                
                String msg = String.format("Thread interrupted: %s", e.getMessage());
                
                JOptionPane.showMessageDialog(this, msg, "Error", 
                    JOptionPane.ERROR_MESSAGE);
            }

            beforeTime = System.currentTimeMillis();
        }
    }
    */

    @Override
    public void run() {
        while (true) {
            cycle();
            repaint();
            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}