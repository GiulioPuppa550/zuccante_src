import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;

public class Board extends JPanel
    implements Runnable {

    // window size
    private final int B_WIDTH = 400;
    private final int B_HEIGHT = 400;

    // star initial position
    private final int INITIAL_X = 200;
    private final int INITIAL_Y = 200;
    private final int FRAME_RATE = 25;

    // for initial velocity
    private final int VEL = 0;

    
    private Thread animator;

    private Image star;
    private int x, y;
    private int vel_x, vel_y;

    public Board() {
        initBoard();
    }

    private void loadImage() {
        ImageIcon ii = new ImageIcon("star.png");
        star = ii.getImage();
    }

    private void initBoard() {

        setBackground(Color.BLACK);
        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));

        // set up mouse events listener 
        addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent e){
                vel_x = (e.getX() - x)/50;
                vel_y = (e.getY() - y)/50;
            }
        });

        loadImage();

        // first position of star
        x = INITIAL_X;
        y = INITIAL_Y;
        // and initial velocity
        vel_x = VEL;
        vel_y = VEL;
    }

    // tis method is called when the Board is attached to the parent JFrame
    @Override
    public void addNotify() {
        super.addNotify();
        animator = new Thread(this);
        animator.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawStar(g);
    }

    private void drawStar(Graphics g) {
        g.drawImage(star, x, y, this);
        Toolkit.getDefaultToolkit().sync();
    }

    private void cycle() {

        // update position
        x += vel_x;
        y += vel_y;

        // boing
        if (x + star.getWidth(null) > B_WIDTH || x < 0) {
            vel_x = -vel_x;
        }
        if (y + star.getHeight(null) > B_HEIGHT || y < 0) {
            vel_y = -vel_y;
        }
        
    }

    // the animation task 
    @Override
    public void run() {
        long beforeTime, timeDiff, sleep;
        beforeTime = System.currentTimeMillis();
        while (true) {
            cycle();
            repaint();
            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = FRAME_RATE - timeDiff;
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                String msg = String.format("Thread interrupted: %s", e.getMessage());
                JOptionPane.showMessageDialog(this, msg, "Error", 
                    JOptionPane.ERROR_MESSAGE);
            }
            beforeTime = System.currentTimeMillis();
        }
    }
    
}