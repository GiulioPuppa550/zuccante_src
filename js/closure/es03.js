// really a closure

function createCounter() {
  let counter = 0
  const myFunction = function() {
    counter = counter + 1
    return counter
  }
  return myFunction
}

let increment = createCounter()
const c1 = increment()
const c2 = increment()
const c3 = increment()
increment = createCounter()
const c4 = increment()

console.log('example increment: ', c1, c2, c3, c4)