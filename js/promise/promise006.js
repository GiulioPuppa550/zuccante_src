let getRandomNumber = (start = 1, end = 10) => {
    return (parseInt(Math.random() * end) % (end - start + 1)) + start;
}

let getRandomPromise = () => {
    return new Promise((resolve, reject) => {
        let randomNumberOfSeconds = getRandomNumber(2, 10);
        setTimeout(() => {
            let randomiseResolving = getRandomNumber(1, 10);
            if (randomiseResolving > 5) {
                resolve({
                    randomNumberOfSeconds: randomNumberOfSeconds,
                    randomiseResolving: randomiseResolving
                });
            } else {
                reject({
                    randomNumberOfSeconds: randomNumberOfSeconds,
                    randomiseResolving: randomiseResolving
                });
            }
        }, randomNumberOfSeconds * 1000);
    });
};

let testProimse = getRandomPromise();
testProimse.then(function (value) {
    console.log("Value when promise is resolved : ", value);
});
testProimse.catch(function (reason) {
    console.log("Reason when promise is rejected : ", reason);
});


for (i = 1; i <= 10; i++) {
    let promise = getRandomPromise();
    promise.then(function (value) {
        console.log("Value when promise is resolved : ", value);
    });
    promise.catch(function (reason) {
        console.log("Reason when promise is rejected : ", reason);
    });
}