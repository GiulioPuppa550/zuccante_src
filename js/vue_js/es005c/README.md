# es005s

QUi solo una piccola modifica rispetto a quanto visto sopra:
- registramio in `emits: ['deleteMemo']` gli eventi emessi e 
- portiamo in un metodo l'emissione
```js
emitEvent(index){
    this.$emit('deleteMemo', index);
    }
```
in modo da poter scrivere
```js
template:
        `<button @click="emitEvent(index)">delete</button> : {{ text }}`
```