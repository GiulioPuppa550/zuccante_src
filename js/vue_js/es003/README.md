# es003

- Il seguente esempio è tratto da [qui](https://www.youtube.com/watch?v=CYPZBK8zUik&list=PL4cUxeGkcC9hYYGbV60Vq3IXYNfDk8At1&index=6).
- Sul tutorial ufficiale "Event Handling" [qui](https://vuejs.org/v2/guide/events.html)
- Gli eventi del mouse, dal DOM, sono descritti [qui](https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent).
- Per accederecalla console con Firefox `More Tools -> Dev Tools`.
- Qui useremop la scorciatoia `v-on:mouseover` sostituito da `@mouseover` così come `v-bind:href="..."` con `:href="..."`.

## v-bind

Molto importante, invitiamo aleggere la *migration guide* [qui](https://v3.vuejs.org/guide/migration/v-bind.html#overview): esso permette il ***binding*** dinamico degli attributi (HTML), la guida nel tutorial la torviamo [qui](https://v3.vuejs.org/guide/class-and-style.html).

## propagazione

Provare a rimuovere `stop` e vedere cosa succede!
