[[_TOC_]]

# Vue js: un'introduzione

Per costruire un'applicazione ***reactive*** *single page* Vue usa il pattern **MVVM**
- ***Model***: oggetti in *plain javascript*.
- ***View Model***: l'oggetto di mezzo coi *DOM listener* e le *directive*
- ***View***: è HTML arricchito con elementi custom 
> i) Gli elementi HTML come oggetti e nuovi elementi custom  
ii) le proprietà di tali elementi   
iii) i metodi per accedere a tali elementi  
iv) eventi anche *custom*   
  