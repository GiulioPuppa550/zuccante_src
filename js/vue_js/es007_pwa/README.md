# es007_pwa

- per installare: `npm install`
- per il server di sviluppo `npm run serve`
- per compilare `npm run build`
- per compilare ed eseguire su di un server in modo da poter usare la pwa `npm run build && npx http-server dist`

## axios come proprietà globale

Invece di importare `axios` nel modulo di utilizzo, più elegantemente definiamo uan prorietà globale  In `main.js`
```js
...
import axios from 'axios'
import './registerServiceWorker'

const app = createApp(App);
app.config.globalProperties.$axios = axios;
app.mount('#app');
```
In caso alternativo possiamo naturalmente importare `axios` solol in un modulo `repository`. Per qualche cenno alle proprietà globali [qui](https://skirtles-code.github.io/vue-examples/patterns/global-properties.html).

## il deployment

Usando un servizio 

https://www.netlify.com/

basta volta compilato con
```
npm run build
```
portare dentro la cartella `dist` con un drag and drop.