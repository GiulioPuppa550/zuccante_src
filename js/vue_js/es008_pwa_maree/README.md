# es008_pwa_maree

- `npm install` per installare
- `npm run serve` per il server di sviluppo
- `npm run build` per compilare
- `http-server dist` usando ul server
- per il deployment https://www.netlify.com/

## i font Google

Abbiamo usato **Open Sans** di Google [qui](https://fonts.google.com/specimen/Open+Sans?query=open+sans): toccare il font desiderato ed importare nel `css`.

## espressioni regolari in Java Script

Usiamo la seguente funzione `s.replace(/^\D*/, ""` che rimpazza il *match* con una stringa vuota, `\D` rappresenta un carattere che non è una cifra numerica, `^` indica inizio stringa. **NB** A noi basta usare, dato il formato del dato
```js
parseFloat(s);
```