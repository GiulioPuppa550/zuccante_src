# MariaDB privilegi

## MariaDB

Procediamo come indicato [qui](https://mariadb.com/kb/en/library/grant/): prima creiamo il DB
``` sql
CREATE DATABASE sequelize;
```
quindi diamo i permessi all'utente (creato come side effect)
``` sql
GRANT ALL ON sequelize.* TO 'sequelize'@'%' IDENTIFIED BY 'sequelize@2019';
```

## sequelize

Paer partire [qui](https://sequelize.org/v5/manual/getting-started.html), l'esempio poi, nei commenti, descrive il resto!