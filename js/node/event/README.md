# eventi

Node si presenta come linguaggio per la programmazione **asincrona** del web. Vediamo di affrontare quindi la gestione degli **eventi**, [qui](https://nodejs.org/dist/latest-v16.x/docs/api/events.html) per la documentazione. Negli esempi riportati prendiamo in considerazione alcuni esempi tratti dalla documentazione.