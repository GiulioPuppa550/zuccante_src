const EventEmitter = require('events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
myEmitter.on('event', (n) => {
  console.log(`event occurred! (${n})`);
});

const getRandomArbitrary = (min, max) => {
  return Math.random() * (max - min) + min;
}

setTimeout(function(){ myEmitter.emit('event', 1);}, getRandomArbitrary(2, 8)*1000);
setTimeout(function(){ myEmitter.emit('event', 2);}, getRandomArbitrary(2, 8)*1000);
setTimeout(function(){ myEmitter.emit('event', 3);}, getRandomArbitrary(2, 8)*1000);
