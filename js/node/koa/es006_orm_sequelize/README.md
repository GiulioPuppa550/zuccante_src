# koa and sequelize: REST adn CRUD


Ecco i pacchetti
- koa
- koa-body
- koa-router
- mariadb
- sequelize

## il DB

Dopo aver impostato la connessione, per il model abbiamo previsto in modulo
``` javascript
module.exports = (sequelize, DataTypes) => {
    const Memo = sequelize.define('memo', {
        title: {type: DataTypes.STRING(20), unique: true},
        description: DataTypes.TEXT
      },
      {
        freezeTableName: true,
      }
    );
    return Memo;
}
```
che importiamo e sincronizziamo
``` javascript
const Memo = sequelize.import(__dirname + '/memo');
Memo.sync();
```


## READ: GET

Proponiamo due forme: la prima
``` javascript
// GET all
router.get('/', async (ctx) => {
    let memos = await Memo.findAll();
    ctx.body = JSON.stringify(memos);
});
```
e la seconda in cui recuperiamo un singolo `Memo`
``` javascript
router.get('/:title', async (ctx) => {
    let title = await ctx.params.title;
    let memo = Memo.findOne({ where: {title: title}});
    ctx.body = JSON.stringify(memo); 
});
```

## CREATE: POST

Qui l'inserimento, ritorniamo l'intero oggetto
``` javascript
router.post('/form', async (ctx) => {
    console.log(ctx.request.body.title);
    let memo = ctx.request.body;
    await Memo.create(memo);
    ctx.body = `created: ${JSON.stringify(memo)}`; 
});
```

## UPDATE: PUT

Noi proponiamo
``` javascript
// PUT: update
router.put('/:title', async (ctx) => {
    let memo = ctx.request.body; 
    await Memo.update({
        description: memo.description
    },
    {
    where: {
        title: memo.title
    }});
    ctx.body = `updated: ${JSON.stringify(memo)}`;
});
```
in alternativa, usando non più il *class method* `update(..)`
``` javascript
router.put('/:title', (ctx) => {
    let memo = await Memo.findOne({where: { title: ctx.params.title }});
    memo = await memo.update(ctx.request.body.memo);
    ctx.body = `updated: ${JSON.stringify(memo)}`;
});
```

## DELETE: DEL

Anche qui la soluzione semplice
``` javascript
router.del('/:title', async (ctx) => {
    let title = ctx.body.title;
    await Memo.destroy({
    where: {
            title: title
    }
    })
    ctx.body = `delete memo with title: ${title}`;  
});
```

## test

Testiamo prima di tutto i **GET**
``` 
curl localhost:3000
curl localhost:3000/titolo
```
con **POST**, il primo *json* il secondo *urlencoded*
```
curl -H "Content-Type: application/json" -d '{"title" : "hello", "description" : "world"}' localhost:3000/form
curl -d 'title=hello&description=world' localhost:3000/form
```
con **PUT**
```
curl -X PUT -d 'title=hello&description=mondo' localhost:3000/form
```
e per il **DELETE**
```
curl -X DELETE localhost:3000/titolo
```


