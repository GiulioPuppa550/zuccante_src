let canvas = document.getElementById('canvas'); 
canvas.width = 400;
canvas.height = 400;
let particles = []; 

function loop() {          
    createParticles();     
    updateParticles();     
    killParticles();     
    drawParticles(); 
} 

function createParticles() {     
    if(particles.length < 100) {             
        particles.push({
            x: Math.random()*canvas.width, 
            y: 0, 
            speed: 2 + Math.random()*3, 
            radius: 5 + Math.random()*5, 
            color: "white",             
        });              
    } 
}

function updateParticles() {    
     for(let i in particles) {         
        let part = particles[i];         
        part.y += part.speed;     
    } 
}

function killParticles() {     
    for(let i in particles) {         
        let part = particles[i];         
        if(part.y > canvas.height) {             
            part.y = 0;         
        }     
    } 
} 

function drawParticles() {     
    let ctx = canvas.getContext('2d');     
    ctx.fillStyle = 'rgb(70,70,121)';     
    ctx.fillRect(0, 0, canvas.width, canvas.height);     
    for(let i in particles) {         
        let part = particles[i];         
        ctx.beginPath();         
        ctx.arc(part.x, part.y, part.radius, 0, Math.PI*2);         
        ctx.closePath();         
        ctx.fillStyle = part.color;         
        ctx.fill();     
    } 
} 



setInterval(loop, 40);