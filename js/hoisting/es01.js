let x = 5; // global

function someThing(y) {
    let z = x + y;
    return z;
}

function anotherThing(y) {
    let x = 17;
    let z = x + y;
    return z;
}

console.log(`someThing(2): ${someThing(2)}`);
console.log(`anotherThing(2): ${anotherThing(2)}`);
