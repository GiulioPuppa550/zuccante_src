var x = 5; // global

function someThing(y) {
    var z = x + y;
    return z;
}

function anotherThing(y) {
    var x = 17;
    var z = x + y;
    return z;
}

console.log(`someThing(2): ${someThing(2)}`);
console.log(`anotherThing(2): ${anotherThing(2)}`);
