import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class DataReader implements Callable {

    static Random rnd = new Random(); 

    @Override
    public String call() {
        int delay = 1 + rnd.nextInt(4);
        System.out.println("Reading data...");
        try {
            TimeUnit.SECONDS.sleep(delay);   
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Data reading finished";
    }

}
