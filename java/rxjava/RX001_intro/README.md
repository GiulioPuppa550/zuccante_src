# RX001_intro

In questo primo esempio facciamo la conoscenza con RXJava. Le API sono [qui](http://reactivex.io/RxJava/2.x/javadoc/).

- **just** crea un observable che emette un solo evento: [qui](http://reactivex.io/documentation/operators/just.html).
- **frm** che in RXJava si specializza emette più eventi da array, liste ecc.:[qui](http://reactivex.io/documentation/operators/from.html).