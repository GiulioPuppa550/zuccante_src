package com.example.es014_authentication_oauth2;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;
import java.util.Set;

@Controller
public class MyController {

    @GetMapping("/")
    public String user(Model model, @AuthenticationPrincipal OAuth2User principal) {
        /*
        String name = principal.getAttribute("name");
        model.addAttribute("name", name);
        */
        Map<String, Object> attributes = principal.getAttributes();
        Set<Map.Entry<String,Object>> entrySet = attributes.entrySet();
        model.addAttribute("entrySet", entrySet);

        return "index";
    }
}