package com.example.es004_jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
public class UserController {

    @Autowired
    private UserRepository repository;

    @PostMapping("/add")
    public void addUser(@RequestBody User user) {
        repository.save(user);
    }

    @PutMapping("/update/{id}")
    public void updateUser(@RequestBody User user, @PathVariable Long id){
        if(repository.findById(id).isPresent()){
            User updated = new User(user.getName(), user.getEmail());
            updated.setId(id);
            repository.save(updated);
        }
    }

    @GetMapping("/findall")
    public List<User> getAllUser() {
        return StreamSupport.stream(repository.findAll().spliterator(), false)
                        .collect(Collectors.toList());
    }

    @GetMapping("/find/{id}")
    public User geUserById(@PathVariable long id) {
        return repository.findById(id).orElse(null);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUserById(@PathVariable long id) {
        repository.deleteById(id);
    }

    @DeleteMapping("/deleteall")
    public void deleteAllUser() {
        repository.deleteAll();
    }

}
