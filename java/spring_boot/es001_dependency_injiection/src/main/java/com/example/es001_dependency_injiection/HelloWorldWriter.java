package com.example.es001_dependency_injiection;

import org.springframework.stereotype.Component;

@Component
public class HelloWorldWriter implements TextWriter{

    private String msg = "Hello World";

    @Override
    public String writeText() {
        return msg;
    }
}
