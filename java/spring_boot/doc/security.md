# Spring Security

Una configurazione minima
```java
@Configuration
@EnableWebSecurity
public class FormLoginSecurityConfig extends WebSecurityConfigurerAdapter {

       @Override
       protected void configure(HttpSecurity http) throws Exception {
               http.authorizeRequests().antMatchers("/**").hasRole("USER").and().formLogin();
       }

       @Override
       protected void configure(AuthenticationManagerBuilder auth) throws Exception {
               auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
       }
}
```

## web configuration

La classe principale è `HttpSecurity`, [qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/config/annotation/web/builders/HttpSecurity.html) per la documentazione
- `authorizeRequests()`: permette gli accessi ristretti
- `antMatchers​(java.lang.String... antPatterns)`: una serie di path, in alternativa a
- `anyRequest()`
- `permitAll()`: a tutti
- `denyAll()`: nessuno
- `authenticated()`: autenticati
- `hasRole​(String )`: riguardo ai ruoli
- `hasAnyRole(String...)`: `USER` o `ADMIN` ad esempio
- `hasAutority`
- `hasAnyAuthority​(String... )`: autorizza uno alla volta `ROLE_USER` o `ROLE_ADMIN`

impostiamo il **login**

- `httpBasic()`: *basic authentication* (vd `json`)
- `formLogin()`: prepara il *login*
. `oauth2Login()`: *login* del *vlient* OAuth2(Google, GitHub, GitLab, Facebook, ecc.)
. `successForwardUrl()`: redireziona fatto il login, di *default* è `/`

la configurazione di **login** `FormLoginConfigurer<...>`

- `loginPage(...)`: imposta una pagina di login (diversa da quella di *default*)
- `passwordParametger()`: di *default* è `password`
- `usernameParameter()`: di default è `userbame`

dopo OAuth2

- `userInfoEndpoint().userService(...)`: configura il service di autenticazione

- and(): ritorna una nuova configurazione da posizionaree in cascate


ai ruoli

[qui](https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/config/annotation/web/configurers/ExpressionUrlAuthorizationConfigurer.AuthorizedUrl.html)


- hasIpAdress("String")
.rememberMe(): vedi [qui](https://www.baeldung.com/spring-security-remember-me)
- not(): nega

[1] "Spring Security Reference" doc ufficiale [qui](https://docs.spring.io/spring-security/site/docs/current/reference/html5/#introduction).  
[1] "Spring Security Architecture" doc ufficiale [qui](https://spring.io/guides/topicals/spring-security-architecture).  
[2] "Spring Security Tutorial" da Java Dev Tutorial [qui](https://www.javadevjournal.com/spring-security-tutorial/)
