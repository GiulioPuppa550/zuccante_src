package com.example.es005_mockito;

public interface HelloService {

    public String get();
}
