package com.example.es005_mockito;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es005MockitoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es005MockitoApplication.class, args);
	}

}
