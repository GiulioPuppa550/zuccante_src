import 'dart:io';

main() {
  HttpServer.bind(InternetAddress.anyIPv6, 8080).then((server) {
    print('server is running');
    server.listen((HttpRequest request) {
      switch (request.method) {
        case 'GET':
          handleGetRequest(request);
          break;
        case 'POST':
        // bla bla bla
      }
    });
  });
}

void handleGetRequest(HttpRequest req) {
  HttpResponse res = req.response;
  res.headers.add(HttpHeaders.contentTypeHeader, "text/html");
  res.write('''
        <!DOCTYPE html>
        <html>
        <head>
          <meta charset="utf-8">
          <title>HelloWorld</title>
        </head>
        <body>
          <h1>Server </h1>
          <p>Hello World</p>
        </body>
        </html>''');
  res.close();
}
