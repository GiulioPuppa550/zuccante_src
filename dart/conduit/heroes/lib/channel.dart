import 'package:heroes/heroes.dart';

import 'heroes_controller.dart';

class HeroesChannel extends ApplicationChannel {
  late ManagedContext context;

  @override
  Future prepare() async {
    logger.onRecord.listen(
        (rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));
    // defining connection
    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final persistentStore = PostgreSQLPersistentStore.fromConnectionInfo(
        "conduit", // user
        "zuccante@2021", // password
        "localhost", // host
        5432, // port
        "conduit01"); // db

    context = ManagedContext(dataModel, persistentStore);
  }

  @override
  Controller get entryPoint {
    final router = Router();

    router.route("/example").linkFunction((request) async {
      return Response.ok({"key": "value"});
    });

    router.route("/heroes/[:id]").link(() => HeroesController(context));

    return router;
  }
}
