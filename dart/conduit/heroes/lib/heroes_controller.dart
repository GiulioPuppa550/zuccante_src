import 'heroes.dart';
import 'model/hero.dart';

class HeroesController extends ResourceController {
  HeroesController(this.context);

  final ManagedContext context;

  // GET
  @Operation.get('id')
  Future<Response> getHeroByID(@Bind.path('id') int id) async {
    final heroQuery = Query<Hero>(context)..where((h) => h.id).equalTo(id);
    final hero = await heroQuery.fetchOne();
    if (hero == null) {
      return Response.notFound();
    }
    return Response.ok(hero);
  }

  @Operation.get()
  Future<Response> getAllHeroes({@Bind.query('name') String? name}) async {
    final heroQuery = Query<Hero>(context);
    if (name != null) {
      heroQuery.where((h) => h.name).contains(name, caseSensitive: false);
    }
    final heroes = await heroQuery.fetch();

    return Response.ok(heroes);
  }

  // POST
  @Operation.post()
  Future<Response> createHero(@Bind.body(ignore: ["id"]) Hero inputHero) async {
    final query = Query<Hero>(context)..values = inputHero;

    final hero = await query.insert();
    return Response.ok(hero);
  }

  // PUT
  @Operation.put()
  Future<Response> updateHeroById(@Bind.body() Hero hero) async {
    final heroQuery = Query<Hero>(context)..where((h) => h.id).equalTo(hero.id);
    final heroFound = await heroQuery.fetchOne();
    // .. no
    if (heroFound == null) {
      return Response.ok({"error": "no_update"});
    }
    // ... yes
    final query = Query<Hero>(context)
      ..values.name = hero.name
      ..where((u) => u.id).equalTo(hero.id);
    await query.update();
    return Response.ok(hero);
  }

  // DELETE
  @Operation.delete()
  Future<Response> deleteHeroByName(
      {@Bind.query('name') required String name}) async {
    final query = Query<Hero>(context)
      ..where((h) => h.name).contains(name, caseSensitive: false);
    final hero = await query.fetchOne();
    if (hero == null) {
      // return Response.notFound();
      return Response.ok({"error": "no_deleted"});
    }
    final int? heroesDeleted = await query.delete();
    return Response.ok({"herosesDeleted": heroesDeleted});
  }

  @Operation.delete('id')
  Future<Response> deleteHeroById(@Bind.path('id') int id) async {
    final query = Query<Hero>(context)..where((h) => h.id).equalTo(id);
    final hero = await query.fetchOne();
    if (hero == null) {
      // return Response.notFound();
      return Response.ok({"error": "no_deleted"});
    }
    final int? heroesDeleted = await query.delete();
    return Response.ok({"herosesDeleted": heroesDeleted});
  }
}
