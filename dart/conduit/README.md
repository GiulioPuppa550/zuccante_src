[[_TOC_]]

# Una breve presentazione a Conduit

L'esercitazione `hero` descrive nel dettaglio la cosyruzione di un server REST; rimandiamo al suo `README.md`.

## Le classi 

**=>** `ApplicationChannel` [qui](https://pub.dev/documentation/conduit/latest/conduit/ApplicationChannel-class.html) per le API  
`Future prepare()` è evdidentemente un metodo di inizializzazione  
`logger` per messaggi di log  
`entryPoint` l'accesso al *routing*

**=>** `Router` [qui](https://pub.dev/documentation/conduit/latest/conduit/Router-class.html) per le API  
`Linkable route(<pattern>)` per il `<pattern>` vedi [qui](https://pub.dev/documentation/conduit/latest/conduit/Router/route.html), ecco alcuni esempi
```
/users
/users/:id
/users/[:id]
/users/:id/friends/[:friendID]
/locations/:name([^0-9])
/files/*
```
ricordiamo che `[:id]` ne indica l'**opzionalità**; possibile l'uso di `*` e di esporessioni regolari.


**=>** `Linkable` [qui](https://pub.dev/documentation/conduit/latest/conduit/Linkable-class.html) per le API , è un'interfaccia per collegarsi ai controller  
`link(..)` per collegarsi come *controller* aò sua **callback**  
`linkable(..)` per passare un `Controller`  

**=>** `Controller` è la classe base [qui](https://pub.dev/documentation/conduit/latest/conduit/Controller-class.html) per le API, implementa `Linkable` (vedi sopra), di solito si usan una sua sottoclasse come quella che segue  

**=>** `ResourceController` [qui](https://pub.dev/documentation/conduit/latest/conduit/ResourceController-class.html) per le API, è il cuore del REST, usa le *annotation* della classe `Operation`

**=>** `Operation` per le ***annotation** REST, [qui](https://pub.dev/documentation/conduit/latest/conduit/Operation-class.html) per le API: ad esempio `@Operation.get('id')` 
tiene conto dell'azione HTTP e delle *path variable*, qui sotto i *const constructor*
```
Operation(String method, [String? pathVariable1, String? pathVariable2, String? pathVariable3, String? pathVariable4])
Operation.delete([String? pathVariable1, String? pathVariable2, String? pathVariable3, String? pathVariable4])
Operation.get([String? pathVariable1, String? pathVariable2, String? pathVariable3, String? pathVariable4])
Operation.post([String? pathVariable1, String? pathVariable2, String? pathVariable3, String? pathVariable4])
Operation.put([String? pathVariable1, String? pathVariable2, String? pathVariable3, String? pathVariable4])
```
`String?` bei parametri richiama all'opzionalità dalla *path variable*.

**=>** `ManagedContext` [qui](https://pub.dev/documentation/conduit/latest/conduit/ManagedContext-class.html) per le API: fornisce un *service* per interrogare il db una volta definito il *model* e la *connessione* al db sullo specifico DBMS (vedi esempio`hero`).

**=>** `Bind` utile per le *annotation*


## introduzione all'ORM

Conduit non usa *raw SQL+ ma 

**=>** `Query<T>` è un oggetto che ci permette di eseguire *query* sul db [qui](https://pub.dev/documentation/conduit/latest/conduit/Query-class.html) per le API, il *generic* `T` si riferisce al *model*

`values`: sono i valori da settare ad inserimento od aggiornamento
```
var q = Query<User>()
    ..values.name = 'Joe'
    ..values.job = 'programmer';
await q.insert();
```
`where` usata in fase di aggiornamento o per filtrare
```
final query = Query<Employee>()
      ..where((e) => e.name).equalTo("Bob");
```
`fetch` e `fetchOne` permettono di recuperare uno o più dati.

