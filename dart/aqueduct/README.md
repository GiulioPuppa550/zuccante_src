# heroes

Questo è l'esempio presentato nella documentazione ufficiale con lievi modifiche e completando le operazioni `REST`. Il tutorial di Aqueduct si può trovare [qui](https://aqueduct.io/docs/tut/getting-started/). Il completamento è stato fatto leggendo le utilissime guide (sempre sulla documentazione).   
**NB** Qui c'è solo il codice!

## Installazione di Aqueduct

Le instruzioni per l'installazione fanno parte del suddetto tutorial. [qui](https://aqueduct.io/docs/getting_started/).

## Installazione di PostgreSQL

Qui ci riferiamo all'installazione su Ubuntu 18.04. Una volta installati i pacchetti possiamo passare all'untente di PostgreSQL
```
sudo su postgres
```
ovvero
``` 
sudo -i -u postgres
```
(l'utente infatti non è attivato) e quindi, solo ora, entrare nella shell.
```
psql
```
Ci spuò connettere nel dattaglio come (dare `man psql`). Possiamo dare
```
SELECT version();
```
Vediamo pra come connetterci al db, per una guida rimandiamo al tutorial ufficiale [qui](https://www.postgresqltutorial.com/psql-commands/).
```
psql -d database -U user
```
se vogliamo connetterci ad un host differente
```
psql -h host -d database -U user
```
in *SSL mode*
```
psql -U user -h host "dbname=db sslmode=require"
```
Se si è entrati in `psql` si può dare
```
\c dbname username
```
Altre modalità grafiche di connessione sono `pgadmin` (web, da installare). Alcuni comandi utili sono:  
- `\l` per il list dei database disponibili
- `\l db` per vedere le caratteristiche di `db`
- `\dt` lista delle tabelle del db in uso
- `\du` per vedere utenti e regole
- `\c dbname` per usare il db
- `\d table_name` per descrivere la tabella
- `\dn` per la lista desgli *schema*
- `\i file` esegue i coman\h ALTER TABLEdi da un file
- `\h ALTER TABLE` help su di un comando `sql`
Per creare utente e db procediamo come segue
```
CREATE DATABASE db_name;
```
per cancellarlo `DROP`. Per creare utenti - [qui](https://www.postgresql.org/docs/8.0/user-manag.html) per una documentazione più estesa
```
CREATE USER davide WITH PASSWORD 'jw8s0F4';
```
anche
```
CREATE USER davide WITH ENCRYPTED PASSWORD 'jw8s0F4';
```
per eliminarlo
```
DROP USER davide
```
per info sugli utenti dal db `postgres`
```
SELECT usename FROM pg_user;
```
`WITH` ci permette di dare ulteriori abilità all'utente, vedi [qui](https://www.postgresql.org/docs/12/sql-createrole.html). Procediamo seguendo il tutorial
```
CREATE DATABASE heroes;
CREATE USER heroes_user WITH CREATEDB;
ALTER USER heroes_user WITH PASSWORD 'password';
GRANT all ON database heroes TO heroes_user;
```
L'opzione `WITH NOCREATEDB` - non abilitato a creare database - è di default quindi non necessaria. Invitiamo anche alla lettura dell'articolo su Medium [qui](https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e). SQL di cui sopra può essere scritto in modo più pulito!
```
CREATE DATABASE yourdbname;
CREATE USER youruser WITH ENCRYPTED PASSWORD 'yourpass';
GRANT ALL PRIVILEGES ON DATABASE yourdbname TO youruser;
```
Fatto questo, dopo aver abilitato `aqueduct`
```
pub global activate aqueduct
```
(`pub` deve essere nel `PATH`) possiamo dare (all'interno della directory del progetto) e seguiamo il tutorial [qui](https://aqueduct.io/docs/tut/getting-started/)
```
aqueduct db generate
```
crea il **migration file** che modifichiamo come da tutorial.

## REST

Usiamo`curl` ([qui](https://curl.haxx.se/docs/httpscripting.html) un tutorial come tanti): per il `POST` scriviamo
```
curl -X POST -H "Content-Type: application/json" -d '{"name":"alghost"}' localhost:8888/heroes
```
per il `PUT`
```
curl -X PUT -H "Content-Type: application/json" -d '{"id":1, "name":"alghost"}' localhost:8888/heroes
```
per il `DELETE`
```
curl -X DELETE localhost:8888/heroes/1
```