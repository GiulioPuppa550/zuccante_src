class A {
  String getMessage() => 'A';
}

class B {
  String getMessage() => 'B';
}

class P {
  String getMessage() => 'P';
}

class AB extends P with A, B {}

class BA extends P with B, A {}

void main() {
  P p = P();
  print(p is P);
  print(p is A);
  print(p is B);
  print('****************');
  
  AB ab = AB();
  print(ab is P);
  print(ab is A);
  print(ab is B);
  print('****************');

  BA ba = BA();
  print(ba is P);
  print(ba is A);
  print(ba is B);
  print('****************');
  
  print(ab.getMessage());
  print(ba.getMessage());
  print('****************');
  
  String result = '';
  result += ab.getMessage();
  result += ba.getMessage();
  print(result);
}
