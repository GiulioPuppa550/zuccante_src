class Veicolo {
  String tipo, marca;
  double _importo;

  Veicolo({this.tipo, this.marca});
  void set importo(v) {
    print('chiamata veicolo');
    this._importo = v;
  }

  num get importo => this.importo;
}

mixin Assicurazione {
  double _importo;

  void set importo(v) {
    print('chiamata mixin');
    this._importo = v;
  }

  num get importo => this.importo;
}

class Autovettura extends Veicolo with Assicurazione {
  Autovettura(marca, importoAssicurazione)
      : super(tipo: 'autovettura', marca: '') {
    super.marca = marca;
    this.importo = importoAssicurazione;
  }
  @override
  String toString() {
    return 'Autovettura di marca ' +
        super.marca +
        ', assicurata per: ' +
        this.importo.toString();
  }
}

main() {
  Autovettura v = Autovettura('FIAT', 600.00);
  print(v.toString());
}
