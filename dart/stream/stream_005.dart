import 'dart:io';

main() async {
  Duration interval = Duration(seconds: 2);
  Stream<int> stream = Stream<int>.periodic(interval, callback);
  await for (int i in stream) {
    if (i > 12) exit(0);
    print(i);
  }
}

int callback(int value) => (value + 1) * 2;
