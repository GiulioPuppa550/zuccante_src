main() async {
  var data = <int>[1, 3, 4, 5, 11, 666];
  var stream = Stream.fromIterable(data); // create the stream

  // subscribe to the streams events
  stream.listen((value) {
    print("Received: $value"); // onData handler
  }); //
}
