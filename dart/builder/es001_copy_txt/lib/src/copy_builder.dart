import 'dart:async';

import 'package:build/build.dart';

class CopyBuilder implements Builder {
  @override
  Map<String, List<String>> get buildExtensions => {
        '.txt': ['.copy.txt']
      };

  @override
  Future<FutureOr<void>> build(BuildStep buildStep) async {
    AssetId inputId = buildStep.inputId;
    print("*** copy from ${inputId.uri.toString()}");
    AssetId copyAssetId = inputId.changeExtension('.copy.txt');
    print("*** copy to ${copyAssetId.uri.toString()}");
    String contents = await buildStep.readAsString(inputId);
    await buildStep.writeAsString(copyAssetId, '''
    ${DateTime.now()}
    =====================
    $contents
    ''');
  }
}
